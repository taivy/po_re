module app

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/douglasmakey/oauth2-example v0.0.0-20190217053201-b19e764d1950 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.1.0
	github.com/jinzhu/gorm v1.9.14
	github.com/sendgrid/rest v2.6.0+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.6.0+incompatible
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
