package main

import (
	"app/pkg/mypkg"
	"fmt"
	"log"
	"net/http"
)

func main() {
	mypkg.SetUpDb()

	// We create a simple server using http.Server and run.
	server := &http.Server{
		Addr:    fmt.Sprintf(":8000"),
		Handler: mypkg.InitHandler(),
	}

	log.Printf("Starting HTTP Server. Listening at %q", server.Addr)
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.Printf("%v", err)
	} else {
		log.Println("Server closed")
	}
}
