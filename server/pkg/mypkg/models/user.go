package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model

	FullName  string `json:"full_name" schema:"full_name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	IsGoogle  bool   `json:"is_google"`
}
