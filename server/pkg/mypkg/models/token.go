package models

import (
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
)

type Token struct {
	UserID             uint
	Email              string
	IsForPasswordReset bool
	*jwt.StandardClaims
}

type ExpiredToken struct {
	gorm.Model
	TokenString string
}
