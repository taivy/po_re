package mypkg

import (
	"app/pkg/mypkg/controllers"
	"app/pkg/mypkg/utils"
	"net/http"

	mux "github.com/gorilla/mux"
)

func SetUpDb() {
	db := utils.DbConn()
	utils.CreateDBTables(db)
}

func InitHandler() http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/", controllers.MainPageHandler)

	api := r.PathPrefix("/api").Subrouter()
	api.HandleFunc("/login", controllers.ApiLoginHandler).Methods("POST")
	api.HandleFunc("/create_user", controllers.ApiNewUserHandler).Methods("POST")

	api.HandleFunc("/send_password_reset_email/{email}", controllers.ApiSendPasswordResetEmailHandler).Methods("GET")

	auth := r.PathPrefix("/auth").Subrouter()

	auth.HandleFunc("/login", controllers.LoginHandlerGet).Methods("GET")
	auth.HandleFunc("/login", controllers.LoginHandlerPost).Methods("POST")
	auth.HandleFunc("/signup", controllers.SignupHandlerGet).Methods("GET")
	auth.HandleFunc("/signup", controllers.SignupHandlerPost).Methods("POST")
	auth.HandleFunc("/google/login", controllers.OauthGoogleLogin)
	auth.HandleFunc("/google/callback", controllers.OauthGoogleCallback)
	auth.HandleFunc("/forgot_password", controllers.ForgotPasswordHandlerGet).Methods("GET")
	auth.HandleFunc("/forgot_password", controllers.ForgotPasswordHandlerPost).Methods("POST")
	auth.HandleFunc("/reset_password/{token}", controllers.ResetPasswordHandlerGet).Methods("GET")
	auth.HandleFunc("/reset_password/{token}", controllers.ResetPasswordHandlerPost).Methods("POST")

	secured := r.PathPrefix("/secured").Subrouter()
	secured.Use(utils.JwtVerify)
	secured.HandleFunc("/sign_out", controllers.SignOutHandlerGet).Methods("GET")
	secured.HandleFunc("/profile/fill_info", controllers.FillInfoHandlerGet).Methods("GET")
	secured.HandleFunc("/profile/fill_info", controllers.FillInfoHandlerPost).Methods("POST")
	secured.HandleFunc("/profile/my_profile", controllers.GetInfoPage).Methods("GET")
	secured.HandleFunc("/api/update_user_info", controllers.ApiUpdateUserInfoHandler).Methods("POST")

	return r
}
