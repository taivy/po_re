package controllers

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path"

	"app/pkg/mypkg/models"
	"app/pkg/mypkg/utils"

	mux "github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

type Context struct {
	FullName  string
	Address   string
	Email     string
	Telephone string
	ErrMsg    string
	HasErr    bool
}

type ApiResp struct {
	Token   string      `json:"token"`
	User    models.User `json:"user"`
	Message string      `json:"message"`
	Status  int         `json:"status"`
}

var (
	BASE_URL = os.Getenv("BASE_URL")
)

func HandleApiErrors(err error, status int, w http.ResponseWriter, r *http.Request, body []byte) {
	if err != nil {
		c := Context{}
		c.ErrMsg = err.Error()
		c.HasErr = true
		LoginHandlerGetWithContext(w, r, c)
	} else if status >= 400 {
		c := Context{}
		respMap := make(map[string]interface{})
		json.Unmarshal(body, &respMap)
		errMsg, ok := respMap["message"].(string)
		if ok {
			c.ErrMsg = errMsg
		} else {
			c.ErrMsg = "Failed to read response body"
		}
		c.HasErr = true
		LoginHandlerGetWithContext(w, r, c)
	}
}

func MainPageHandler(w http.ResponseWriter, r *http.Request) {
	c := Context{}

	templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
		path.Join("templates", "main_page.html")))

	if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
		http.Error(w, http.StatusText(500), 500)
	}
	// prevent "Back" button when unlogin
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")
}

func ForgotPasswordHandlerGet(w http.ResponseWriter, r *http.Request) {
	c := Context{}

	templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
		path.Join("templates", "ask_email.html")))

	if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
		http.Error(w, http.StatusText(500), 500)
	}

}

func ForgotPasswordHandlerPost(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}

	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Error pasring form", 400)
		return
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(user, r.Form)
	if err != nil {
		http.Error(w, "Error decoding", 400)
		return
	}
	resp, err := http.Get(BASE_URL + "/api/send_password_reset_email/" + user.Email)
	status := resp.StatusCode
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil || status >= 400 {
		HandleApiErrors(err, status, w, r, body)
	} else {
		c := Context{}
		templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
			path.Join("templates", "sent_restore_pass_email.html")))
		if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
			http.Error(w, http.StatusText(500), 500)
		}
	}

}

func ResetPasswordHandlerGet(w http.ResponseWriter, r *http.Request) {
	c := Context{}

	vars := mux.Vars(r)
	tokenStr := vars["token"]

	claims := &models.Token{}

	claims, tkn, err := utils.ParseToken(tokenStr)
	if err != nil || !tkn.Valid || !claims.IsForPasswordReset {
		http.Error(w, "Bad token", 400)
		return
	}

	templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
		path.Join("templates", "new_password.html")))

	if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
		http.Error(w, http.StatusText(500), 500)
	}
}

func ResetPasswordHandlerPost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	tokenStr := vars["token"]

	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Error pasring form", 400)
		return
	}

	password := r.FormValue("password")

	claims := &models.Token{}

	claims, tkn, err := utils.ParseToken(tokenStr)
	if err != nil || !tkn.Valid || !claims.IsForPasswordReset {
		http.Error(w, "Bad token", 400)
		return
	}

	utils.SaveJwtToken(w, r, tokenStr)
	user, err := utils.FindUserByEmail(claims.Email)
	if user == nil {
		http.Error(w, "No user found", 404)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	pass, err := utils.CreatePassword(password)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	user.Password = string(pass)

	db.Save(&user)

	http.Redirect(w, r, "/secured/profile/my_profile", 302)

}

func LoginHandlerGetWithContext(w http.ResponseWriter, r *http.Request, c Context) {
	templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
		path.Join("templates", "login.html")))

	if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
		http.Error(w, http.StatusText(500), 500)
	}
}

func LoginHandlerGet(w http.ResponseWriter, r *http.Request) {
	c := Context{}
	LoginHandlerGetWithContext(w, r, c)
}

func LoginHandlerPost(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}

	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Error pasring form", 400)
		return
	}

	decoder := schema.NewDecoder()
	err = decoder.Decode(user, r.Form)
	if err != nil {
		http.Error(w, "Error decoding", 400)
		return
	}

	b, err := json.Marshal(user)

	status, body, err := utils.MakePostRequest(BASE_URL+"/api/login", b, "application/json", "")

	if err != nil || status >= 400 {
		HandleApiErrors(err, status, w, r, body)
	} else {
		c := Context{}
		resp := ApiResp{}
		err := json.Unmarshal(body, &resp)
		if err != nil {
			c.ErrMsg = err.Error()
			c.HasErr = true
			LoginHandlerGetWithContext(w, r, c)
			return
		}

		tokenString := resp.Token
		utils.SaveJwtToken(w, r, tokenString)

		http.Redirect(w, r, "/secured/profile/my_profile", 302)
	}
}

func SignupHandlerGet(w http.ResponseWriter, r *http.Request) {
	c := Context{}

	templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
		path.Join("templates", "sign_up.html")))

	if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
		http.Error(w, http.StatusText(500), 500)
	}
}

func CreateUser(w http.ResponseWriter, r *http.Request, user *models.User) {
	b, err := json.Marshal(user)

	status, body, err := utils.MakePostRequest(BASE_URL+"/api/create_user", b, "application/json", "")

	if err != nil || status >= 400 {
		HandleApiErrors(err, status, w, r, body)
	} else {
		c := Context{}
		resp := ApiResp{}
		err := json.Unmarshal(body, &resp)
		if err != nil {
			c.ErrMsg = err.Error()
			c.HasErr = true
			LoginHandlerGetWithContext(w, r, c)
			return
		}

		tokenString := resp.Token
		utils.SaveJwtToken(w, r, tokenString)

		http.Redirect(w, r, "/secured/profile/fill_info", 302)
		// use 307 to keep method
		//http.Redirect(w, r, "/secured/profile/fill_info", http.StatusTemporaryRedirect)
	}
}

func SignupHandlerPost(w http.ResponseWriter, r *http.Request) {
	// TODO: should check if user with email already exists and throw error if it does

	user := &models.User{}

	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Error pasring form", 400)
		return
	}

	decoder := schema.NewDecoder()
	err = decoder.Decode(user, r.Form)
	if err != nil {
		http.Error(w, "Error decoding", 400)
		return
	}
	CreateUser(w, r, user)
}

func FillInfoHandlerPost(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}

	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Error pasring form", 400)
		return
	}

	decoder := schema.NewDecoder()
	err = decoder.Decode(user, r.Form)
	if err != nil {
		http.Error(w, "Error decoding", 400)
		return
	}

	token, ok := r.Context().Value("user").(models.Token)
	if !ok {
		http.Error(w, "Failed to get token", 400)
	}

	targetUser := FindUserByToken(token)
	if targetUser == nil {
		http.Error(w, "Failed to find user by token", 400)
	}

	if user.Email != "" {
		targetUser.Email = user.Email
	}

	targetUser.FullName = user.FullName
	targetUser.Address = user.Address
	targetUser.Telephone = user.Telephone

	b, err := json.Marshal(targetUser)

	tokenStr, ok := r.Context().Value("token").(string)
	status, body, err := utils.MakePostRequest(BASE_URL+"/secured/api/update_user_info", b, "application/json", tokenStr)

	if err != nil || status >= 400 {
		HandleApiErrors(err, status, w, r, body)
	} else {
		http.Redirect(w, r, "/secured/profile/my_profile", 302)
	}

}

func FindUserByToken(token models.Token) *models.User {
	user := &models.User{}
	user, _ = utils.FindUserByEmail(token.Email)
	return user
}

func FillInfoHandlerGet(w http.ResponseWriter, r *http.Request) {
	c := Context{}

	token, ok := r.Context().Value("user").(models.Token)
	if !ok {
		http.Error(w, "Failed to get token", 400)
	}

	user := FindUserByToken(token)
	if user == nil {
		http.Error(w, "Failed to find user by token", 400)
	}

	c.FullName = user.FullName
	c.Address = user.Address
	c.Email = user.Email
	c.Telephone = user.Telephone

	templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
		path.Join("templates", "fill_info.html")))

	if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
		http.Error(w, http.StatusText(500), 500)
	}
}

func GetInfoPage(w http.ResponseWriter, r *http.Request) {
	c := Context{}

	token, ok := r.Context().Value("user").(models.Token)
	if !ok {
		http.Error(w, "Failed to get token", 400)
	}

	user := FindUserByToken(token)
	if user == nil {
		http.Error(w, "Failed to find user by token", 400)
	}

	c.FullName = user.FullName
	c.Address = user.Address
	c.Email = user.Email
	c.Telephone = user.Telephone

	templates := template.Must(template.ParseFiles(path.Join("templates", "layout.html"),
		path.Join("templates", "info_page.html")))

	if err := templates.ExecuteTemplate(w, "layout", c); err != nil {
		http.Error(w, http.StatusText(500), 500)
	}
}

func SignOutHandlerGet(w http.ResponseWriter, r *http.Request) {
	var db = utils.DbConn()

	token, ok := r.Context().Value("token").(string)
	if !ok {
		http.Error(w, "Failed to get token", 400)
	}

	expiredToken := models.ExpiredToken{TokenString: token}
	db.Save(&expiredToken)

	cookie := http.Cookie{
		Name:   "token",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(w, &cookie)
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")

	http.Redirect(w, r, "/", 302)
}
