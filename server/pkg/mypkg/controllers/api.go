package controllers

import (
	"app/pkg/mypkg/models"
	"app/pkg/mypkg/utils"
	"encoding/json"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

type error interface {
	Error() string
}

var JWT_SECRET = os.Getenv("JWT_SECRET")

var db = utils.DbConn()

func ApiLoginHandler(w http.ResponseWriter, r *http.Request) {
	user_ := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user_)
	if err != nil {
		var resp = map[string]interface{}{"status": 400, "message": "Invalid request"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(resp)
	}
	resp, user := FindOne(user_.Email, user_.Password)

	tokenString, err := utils.CreateJWTToken(user, false)
	if err != nil {
		var resp = map[string]interface{}{"status": 400, "message": "Failed to create JWT token"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["token"] = tokenString

	w.WriteHeader(resp["status"].(int))
	json.NewEncoder(w).Encode(resp)
}

func FindOne(email, password string) (map[string]interface{}, *models.User) {
	user, err := utils.FindUserByEmail(email)
	if err != nil {
		var resp = map[string]interface{}{"status": 404, "message": "Email address not found"}
		return resp, user
	}

	errf := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if errf != nil && errf == bcrypt.ErrMismatchedHashAndPassword {
		var resp = map[string]interface{}{"status": 400, "message": "Invalid login credentials. Please try again"}
		return resp, user
	}

	var resp = map[string]interface{}{"status": 200, "message": "logged in"}
	resp["user"] = user
	return resp, user
}

func ApiNewUserHandler(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(user)

	pass, err := utils.CreatePassword(user.Password)
	if err != nil {
		var resp = map[string]interface{}{"status": 400, "message": "Password Encryption failed"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(resp)
		return
	}

	user.Password = string(pass)

	createdUser := db.Create(user)
	var errMessage = createdUser.Error

	if createdUser.Error != nil {
		var resp = map[string]interface{}{"status": 400, "message": errMessage}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(resp)
		return
	}
	tokenString, err := utils.CreateJWTToken(user, false)
	if err != nil {
		var resp = map[string]interface{}{"status": 400, "message": "Failed to create JWT token"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(resp)
		return
	}
	var resp = map[string]interface{}{"status": 200, "message": "created user"}
	resp["user"] = createdUser
	resp["token"] = tokenString

	json.NewEncoder(w).Encode(resp)
}

func ApiUpdateUserInfoHandler(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		var resp = map[string]interface{}{"status": 400, "message": "Invalid request"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(resp)
	}
	db.Save(&user)
	json.NewEncoder(w).Encode(&user)
}

func ApiSendPasswordResetEmailHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]

	user, err := utils.FindUserByEmail(email)
	if err != nil {
		var resp = map[string]interface{}{"status": 404, "message": "user not found"}
		w.WriteHeader(404)
		json.NewEncoder(w).Encode(resp)
		return
	}
	token, err := utils.CreateJWTToken(user, true)
	if err != nil {
		var resp = map[string]interface{}{"status": 404, "message": err.Error()}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(resp)
		return
	}

	link := BASE_URL + "/auth/reset_password/" + token
	err = utils.SendPasswordResetEmail(email, user, link)
	if err != nil {
		var resp = map[string]interface{}{"status": 404, "message": err.Error()}
		json.NewEncoder(w).Encode(resp)
		return
	}

	var resp = map[string]interface{}{"status": 200, "message": "sent email"}
	json.NewEncoder(w).Encode(resp)
}
