package utils

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

func MakePostRequest(url string, payload []byte, contentType string, token string) (int, []byte, error) {
	request, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	if err != nil {
		return -1, nil, err
	}
	if contentType == "" {
		contentType = "application/json"
	}

	request.Header.Set("Content-Type", contentType)
	if token != "" {
		request.Header.Set("X-JWT-Token", token)
	}

	client := &http.Client{}
	resp, err := client.Do(request)

	if err != nil {
		return -1, nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	return resp.StatusCode, body, err
}
