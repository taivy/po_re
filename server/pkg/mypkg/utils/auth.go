package utils

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"app/pkg/mypkg/models"

	jwt "github.com/dgrijalva/jwt-go"
)

var (
	JWT_SECRET = os.Getenv("JWT_SECRET")
)

func CheckTokenIsExpired(token string) bool {
	var db = DbConn()
	tokenExpired := models.ExpiredToken{}
	db.Where("token_string = ?", token).First(&tokenExpired)
	return tokenExpired.TokenString != ""
}

func ParseToken(tokenString string) (*models.Token, *jwt.Token, error) {
	claims := &models.Token{}
	tkn, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(JWT_SECRET), nil
	})
	return claims, tkn, err
}

// JwtVerify Middleware function
func JwtVerify(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var tknStr string

		tknStr = r.Header.Get("X-JWT-Token")
		if tknStr == "" {
			// Get the JWT string from the cookie
			// We can obtain the session token from the requests cookies, which come with every request
			c, err := r.Cookie("token")
			if err != nil {
				if err == http.ErrNoCookie {
					// If the cookie is not set, return an unauthorized status
					w.WriteHeader(http.StatusUnauthorized)
					return
				}
				fmt.Println("cookie err")
				fmt.Println(err)

				// For any other type of error, return a bad request status
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			tknStr = c.Value
		}

		tokenIsExpired := CheckTokenIsExpired(tknStr)
		if tokenIsExpired {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		// Initialize a new instance of `Claims`

		// Parse the JWT string and store the result in `claims`.
		// Note that we are passing the key in this method as well. This method will return an error
		// if the token is invalid (if it has expired according to the expiry time we set on sign in),
		// or if the signature does not match
		claims, tkn, err := ParseToken(tknStr)
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			fmt.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if !tkn.Valid {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "user", *claims)
		ctx = context.WithValue(ctx, "token", tknStr)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func CreateJWTToken(user *models.User, isForPasswordReset bool) (string, error) {
	// it should be 5 minutes and refreshed when expired but i don't have time to implement
	expiresAt := time.Now().Add(time.Minute * 100000).Unix()

	tk := &models.Token{
		UserID: user.ID,
		Email:  user.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
		IsForPasswordReset: isForPasswordReset,
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	tokenString, err := token.SignedString([]byte(JWT_SECRET))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func SaveJwtToken(w http.ResponseWriter, r *http.Request, tokenString string) {
	// it should be 5 minutes and refreshed when expired but i don't have time to implement
	expirationTime := time.Now().Add(100000 * time.Minute)

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Path:    "/",
		Expires: expirationTime,
	})
}
