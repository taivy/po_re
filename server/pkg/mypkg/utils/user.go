package utils

import (
	"app/pkg/mypkg/models"

	"golang.org/x/crypto/bcrypt"
)

var db = DbConn()

func FindUserByEmail(email string) (*models.User, error) {
	user := &models.User{}
	err := db.Where("Email = ?", email).First(user).Error
	return user, err
}

func CreatePassword(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}
