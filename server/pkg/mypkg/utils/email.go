package utils

import (
	"app/pkg/mypkg/models"
	"errors"
	"log"
	"os"
	"strconv"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

var (
	SENDGRID_API_KEY   = os.Getenv("SENDGRID_API_KEY")
	SENDGRID_SEND_FROM = os.Getenv("SENDGRID_SEND_FROM")
)

func SendPasswordResetEmail(email string, user *models.User, link string) error {
	from := mail.NewEmail("Example User", SENDGRID_SEND_FROM)
	subject := "Your password reset link"
	to := mail.NewEmail(email, email)
	plainTextContent := "Link: " + link
	htmlContent := `<strong><a href="` + link + `">Here's the link</a></strong>`
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(SENDGRID_API_KEY)
	response, err := client.Send(message)
	if err != nil {
		log.Println(err)
	} else {
		log.Println(response.StatusCode)
		log.Println(response.Body)
		log.Println(response.Headers)
	}
	if response.StatusCode >= 400 {
		return errors.New("SendGrid response status code: " + strconv.Itoa(response.StatusCode))
	}
	return err
}
