package utils

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"app/pkg/mypkg/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	MYSQL_HOST = os.Getenv("MYSQL_HOST")
	MYSQL_PORT = os.Getenv("MYSQL_PORT")
	MYSQL_USER = os.Getenv("MYSQL_USER")
	MYSQL_PASS = os.Getenv("MYSQL_PASS")
	MYSQL_DB   = os.Getenv("MYSQL_DB")
)

func CreateDBTables(db *gorm.DB) {
	db.Set("gorm:table_options", "CHARSET=utf8mb4").CreateTable(&models.User{})
	db.Set("gorm:table_options", "CHARSET=utf8mb4").CreateTable(&models.ExpiredToken{})
}

func DbConn() *gorm.DB {
	MYSQL_PORT_int, err := strconv.Atoi(MYSQL_PORT)
	if err != nil {
		log.Fatal(err)
	}
	connArgs := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", MYSQL_USER, MYSQL_PASS, MYSQL_HOST, MYSQL_PORT_int, MYSQL_DB)
	db, err := gorm.Open("mysql", connArgs)
	if err != nil {
		log.Fatal(err)
	}
	return db
}
